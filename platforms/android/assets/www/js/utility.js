angular.module('boosterapp.utility', ['ionic'])

	.service('Utility', function($ionicPopup, $q, $rootScope, $state, $cordovaToast, Booster) {

    var $rs = $rootScope;

    //var self = this;    
    var debug = 3; // debug level - higher means more verbose messages
	$rs.popup = function(title, message) {	    
        console.log('popup:title='+title+' ;message='+message);
        /*
		 $ionicPopup.show({
		     title: title,
		     template: message
		 });
        */
	}

    $rs.goToState=function(theState) {
        $state.go(theState);
    }

    $rs.doDelayed=function(f1, delay, args)
    {
        console.log('doDelayed:args='+JSON.stringify(args));
        function sleep(millis, callback) {
            setTimeout(function()
                    { callback(); }
            , millis);
        }
        function foobar_cont(){
            console.log("foobar_cont");
            console.log('doDelayed:foobar_cont:args='+JSON.stringify(args));
            f1.apply(this, args);
        };
        sleep(delay, foobar_cont);    
    }

    $rs.fbShare=function(selfie){
        var d = $q.defer();
      console.log('$rs.fbShare:selfie.UserId='+selfie.UserId);
      $rootScope.trackEvent("fbShare");

      var message = selfie.message,//selfie.BoostTitle,
      subject = 'Booster App',
      fileOrFileArray = "data:image/png;base64," + selfie.ImageData ,
      url = "http://signup.theboosterapp.com";

      var sharing = window.plugins.socialsharing;
      sharing.share(
      message,
      subject,
      fileOrFileArray,
      url,
      function(result) {
        $rootScope.trackEvent('User shared');                          
        console.log('Sharing success: ' + JSON.stringify(result));
        d.resolve(result);
        return d.promise;
      },
      function(result) {
        $rootScope.trackError(error, "fbShare");
        console.log('Sharing error: ' + JSON.stringify(result));
        d.reject(result);
        return d.promise;
      });
      return d.promise;
    }

    //FB login button click event
    $rs.promiseFacebookLogin = function() 
    {
        console.log('promiseFacebookLogin');
        var fbId = $rs.getFBUserId();
        var fbLr = $rs.fbLr;
        var d = $q.defer();
        console.log('promiseFacebookLogin:2:fbLr='+$rs.jStr(fbLr));
        //var fbId = window.localStorage['fbid'];
        if($rs.varIsSet(fbId) && $rs.varIsSet(fbLr)) 
        {
            //mixpanel.identify(fbId);
            console.log('promiseFacebookLogin:resolve:fbId='+fbId);
            console.log('promiseFacebookLogin:resolve:fbLr='+fbLr);
            d.resolve(fbId);
            return d.promise;
        }

        /*
        console.log('window.cordova='+window.cordova);
        if (!window.cordova) {
            //facebookConnectPlugin = fbcpWeb;
            facebookConnectPlugin.browserInit("625654127541709");
            // version is optional. It refers to the version of API you may want to use.
        }
        */
 
        //alert('promiseFacebookLogin:call:facebookConnectPlugin');
        facebookConnectPlugin.
        login
        (
            ['public_profile', 'user_friends', 'email'], 
            // SUCCESS
            function(res) 
            {
                console.log("promiseFacebookLogin:res=" + $rs.jStr(res));
                $rs.setFbLoginResponse(res);
                d.resolve(res);
                return d.promise;
        }, 
            function(err) 
            {
                var errLog = 'promiseFacebookLogin:facebookConnectPlugin:err'+err;
                console.log(errorLog);
                d.reject(errLog);
                return d.promise;
            }       
        )
        console.log('promiseFacebookLogin:end');
        return d.promise;
    };

    $rs.promiseGetLoginStatus = function()
    {
        console.log('promiseGetLoginStatus');
        var d = $q.defer();
        console.log('promiseGetLoginStatus:d='+d);
        //d.notify('promiseGetLoginStatus');

        var fbLr = $rs.fbLr;
        console.log('promiseGetLoginStatus:fbLr='+fbLr);
        if($rs.varIsSet(fbLr)) 
        {
            d.resolve(fbLr);
            return d.promise;
        }

        /*
        console.log('window.cordova='+window.cordova);
        if (!window.cordova) {
            //facebookConnectPlugin = fbcpWeb;
            facebookConnectPlugin.browserInit("625654127541709");
            // version is optional. It refers to the version of API you may want to use.
        }
        */

        facebookConnectPlugin.getLoginStatus
        (
            function(res)
            {
                var fbId;
                console.log('promiseGetLoginStatus:getLoginStatus:res='+$rs.jStr(res));
                if(res.status + "" == "connected")
                { 
                    fbId = res.authResponse.userID;
                    if($rs.varIsSet(fbId))
                    {
                        $rs.setFBUserId(fbId);
                        console.log('promiseGetLoginStatus:getLoginStatus:resolve:fbId='+fbId);
                        d.resolve(res);
                        return d.promise;
                }
                    else
                    {
                        console.log('promiseGetLoginStatus:getLoginStatus:reject:fbId=='+fbId);
                        d.reject();
                        return d.promise;
                    }
                } else
                {
                    console.log('promiseGetLoginStatus:getLoginStatus:reject:connected==false');
                    d.reject();
                    return d.promise;
                }
            },
            function(err){
                console.log('facebookConnectPlugin.getLoginStatus:err='+err);
            }
        )
        console.log('promiseGetLoginStatus:getLoginStatus:return');
        return d.promise;
    };

    $rs.promiseGetProfile = function()
    {
        console.log('promiseGetProfile');
        var d = $q.defer();
        var fbId = $rs.getFBUserId();
        console.log('promiseGetProfile:fbId='+fbId);
        if(!$rs.varIsSet(fbId))
        {
            console.log('promiseGetProfile:reject:fbId='+fbId);
            d.reject('fbId='+fbId);
            return d.promise;
        }
        Booster.getProfileByFbId(fbId)
        .then(function(result)
        {
            console.log('promiseGetProfile:getProfileByFbId:result='+$rs.jStr(result));
            $rs.getProfileRes = result;
            if($rs.varIsSet(result.data))
            {
                console.log('promiseGetProfile:getProfileByFbId:resolve');
                $rs.setProfileFromWs(result);                
                d.resolve(result);
                return d.promise;
            }
            else
            {
                console.log('promiseGetProfile:getProfileByFbId:reject');
                d.reject(result);
                /*
                console.log('promiseGetProfile:return:promiseRegister');
                return $rs.promiseRegister()
                .then(function(result)
                {
                    console.log('promiseGetProfile:return:promiseGetProfile');
                    return $rs.promiseGetProfile();
                    //d.reject(result);
                });
                */
            }
        }).catch(function(err){
            console.log('catch:promiseGetProfile:getProfileByFbId:reject');
            d.reject(err);
            return d.promise;
        });
        return d.promise;
    }

    $rs.promiseRegister = function()
    {
        var d = $q.defer();
        /*
        if(window.localStorage['isRegistered']=="true")
        {
            d.resolve();
            return d.promise;
        }
        */
        var fbLr = $rs.fbLr;
        //var fbMe = $rs.fbGraphMeRes;
        if(!$rs.varIsSet(fbLr))
        {
            console.log('promiseRegister:reject:fbLr='+fbLr);
            d.reject(data);
            return d.promise;
        }
        var data = fbLr.authResponse;
        console.log('promiseRegister:data='+$rs.jStr(data));
        Booster.register(data.id, data.first_name, data.last_name, data.email, data.accessToken,"")
        .then(function(result)
        {
            console.log('promiseRegister:register:result='+$rs.jStr(result));
            if(result.data.IsSuccess==true)
            {   
                window.localStorage['isRegistered']=true;
                console.log('promiseRegister:register:resolve');
                $rs.getFriends().then(function(result){
                     Booster.friendJoined(data.first_name+' '+data.last_name, result);
                })
                d.resolve(result);
            }
            else
            {
                console.log('promiseRegister:register:reject');
                d.reject(result);
            }
        },
        function(error){
            console.log('promiseRegister:register:reject:error='+error);
            d.reject(error);
        });
        d.reject();
        return d.promise;
    }

    $rs.doPromiseStartAppOnce = function()
    {
        //alert('doPromiseStartAppOnce');
        console.log('doPromiseStartAppOnce');
        var d = $q.defer();
        if($rs.alreadyStarting == true)
        {
            window.localStorage['isFirstEntrance'] = false;
            d.reject($rs.alreadyStarting);
            return false;
        }
        $rs.alreadyStarting = true;
        // TODO: need this but js wasn't finding it, even though I'm merely defining it here
        $rs.startAppCallCount = 1;
        //alert('doPromiseStartAppOnce:startAppCallCount='+startAppCallCount);
        //console.log('doPromiseStartAppOnce:startAppCallCount='+startAppCallCount);
        window.localStorage['isFirstEntrance'] = false;

        //GAB: works to speed up upcoming but shows error, missing profile info
        //$state.go('app.upcoming');

        return $rs.promiseStartApp();
        return d.promise;
    }

    // RECURSIVE WORK BACKWARDS
    $rs.promiseStartApp = function()
    {
        //alert('promiseStartApp');
        var d = $q.defer();
        var doStart = true;
        console.log('promiseStartApp - promiseGetProfile')
        return $rs.promiseGetProfile()
        .catch(function(err){
            doStart=false;
            console.log('promiseStartApp - promiseRegister')
            return $rs.promiseRegister()
        // }).catch(function(err){
            // doStart=false;
            // console.log('promiseStartApp - promiseGetFbGraphMe')
            // return $rs.promiseGetFbGraphMe()
        }).catch(function(err){
            doStart=false;
            console.log('promiseStartApp - promiseFacebookLogin')
            return $rs.promiseFacebookLogin()
        }).finally(function(result){
            console.log('promiseStartApp - doStart='+doStart);
            if(doStart) {
                console.log('promiseStartApp - resolve:call:startUpcoming='+result);
                $rs.startUpcoming();
                if($rs.varIsSet($rs.pushState))
                {
                    $state.go($rs.pushState);  
                    $rs.pushState = "";    
                }
                $rs.isAppStarted = true;

                d.resolve(result);
                return d.promise;
            } else {
                // RECURSE
                console.log('promiseStartApp - call:self:RECURSE:result='+result);
                // TODO, some prob with this startAppCallCount variable
                if($rs.startAppCallCount > 10)
                {
                    alert('promiseStartApp - call:self:RECURSE:startAppCallCount='+startAppCallCount+' result='+result);
                    d.reject(result);
                    return d.promise;
                }
                $rs.startAppCallCount++;
                $rs.promiseStartApp();
            }
        });
        return d.promise;
    };

    /*
    $rs.promiseStartAppProc = function()
    {
        console.log('promiseStartApp');
        var d = $q.defer();
        return $rs.promiseGetLoginStatus()
        .catch(function(err){
            return $rs.promiseFacebookLogin()
        //}).then(function(result){
        //    return $rs.promiseGetFbGraphMe()
        }).then(function(result){
            return $rs.promiseGetProfile()
        //}).then(function(result){
        //    return $rs.promiseAddDefaultBooster()
        }).then(function(result){
            console.log('promiseStartApp:resolve:result='+result);
            $rs.startUpcoming();
            d.resolve(result);
            return d.promise;
        }).catch(function(err){
            console.log('promiseStartApp:reject:err='+err);
        });
        return d.promise;
    };
    */

    $rs.startUpcoming = function() 
    {
        console.log('startUpcoming');
        var getProfileWsr = $rs.getProfileFromWs();
        getProfileWsr = JSON.parse(getProfileWsr);
        console.log('startUpcoming:getProfileWsr='+$rs.jStr(getProfileWsr));
        $rs.setUserId(getProfileWsr.data.UserId);
        $rs.updateDeviceId();
        $rs.setProfileFromWs(getProfileWsr);                
        $state.go('app.upcoming');
    }

    $rs.varIsSet = function(x)
    {
        if(x == "" || x == null || x == undefined || x == "null" || x == "undefined")
        {
            return false;
        }
        return true;
    }

    /*
    $rs.promiseGetFbGraphMe = function() {
        var d = $q.defer();
        console.log('promiseGetFbGraphMe');
        var d = $q.defer();
        var fbId = $rs.getFBUserId();
        if(!$rs.varIsSet(fbId))
        {
            console.log('promiseGetFbGraphMe:reject:fbId='+fbId);
            d.reject(fbId);
            return d.promise;
        }
        console.log('promiseGetFbGraphMe:fbId='+fbId);
        facebookConnectPlugin.api(fbId + "?fields=id,name,first_name,last_name,email,picture",
            ['public_profile'],
        function (result) {
            console.log("promiseGetFbGraphMe:api:result="+$rs.jStr(result));
            $rs.fbGraphMeRes=result;
            d.resolve(result);
            return d.promise;
        },
        function (error) {
            console.log('promiseGetFbGraphMe:api:error='+error);
            d.reject(error);
            return d.promise;
        });
        console.log('promiseGetFbGraphMe:return');
        return d.promise;
    };
    */

    //Update device id  
    $rs.updateDeviceId=function(){    
        var userId = $rs.getUserId();
        //console.log('updateDeviceId:userId='+userId);
        $rs.trackEvent("updateDeviceId");
        var did = $rs.getDeviceId();
        console.log('updateDeviceId:userId'+userId+' did='+did);
        Booster.updateDeviceId(userId, did).then(function(response) {                   
                    //Device updated successfully
                    console.log('updateDeviceId:response='+response);
                    var test =response;
                  },
                  function( errorMessage ) {                
                    $rs.trackError(errorMessage, "updateDeviceId");
                    console.log('updateDeviceId:errorMessage='+$rs.jStr(errorMessage));
                    //$rs.popup("Error", "Error occured in updateDeviceId");
                  }

        );
    };

    $rs.updateNotifications= function(){
        console.log('updateNotifications');
        Booster.getNotification($rs.getUserId()).then(function(result) { 
            $rs.notef.count =result.data.UnReadCount; 
            $rs.notef.list =result.data.Notifications;      
          },
          function( errorMessage ) {
            $rs.popup("Error", "Error: updateNotifications");
        });   

        $rs.updateRequestCount();
    }

    $rs.updateRequestCount=function(){
         //Update request count 
        Booster.getRequestBoostCount($rs.getUserId()).then(function(result) { 
            $rs.requestCount =result.data; 
               
          },
          function( errorMessage ) {
            $rs.popup("Error", "Error: updateRequestCount");
        }); 

    }

    $rs.setDevicePlatform=function(dp)
    {
        $rs.devicePlatform=dp;
    }

    $rs.getDevicePlatform=function()
    {
        //console.log('getDevicePlatform='+$rs.devicePlatform);
        return $rs.devicePlatform;
    }

    $rs.trackEvent = function(act) {
        var d = "x";
        d = $rs.getDevicePlatform();
        if(d == "Android") d = "A";
        if(d == "iOS") d = "i";

        mixpanel.track(d+":"+act);

        //GA 
        ga('send', 'screenview', {
          'appName': 'BoosterClassic',
          'appId': 'myAppId',
          'appVersion': '1.0',
          'appInstallerId': 'myInstallerId',
          'screenName': act
        });
    }

    $rs.bulog = function(msg) {
        console.log("ul:"+msg);
        /*
        $cordovaToast.showShortTop('Here is a message').then(function(success) {
            // success
        }, function (error) {
            // error
        });
        */        
    }

    $rs.trackError = function(act) {
        $rs.trackEvent("Error:"+act);
    }

    $rs.trackUserError = function(act) {
        $rs.trackEvent("UserError:"+act);
    }

    $rs.getFBUserId = function(){        	       	
        return window.localStorage['fbid']; 
    };
        
    $rs.setFBUserId = function(fbid){     
        //console.log('setFBUserId:fbid='+fbid);   	
        window.localStorage['fbid'] = fbid;              
    };

    $rs.getFbFriends = function(){     
        var fbFriends = window.localStorage['fbFriends'];             
        //console.log('getFbFriends:fbFriends='+fbFriends);             
        return fbFriends;  
    };

    $rs.setFbFriends = function(fbFriends){     
        //console.log('setFbFriends:fbFriends='+$rs.jStr(fbFriends));             
        window.localStorage['fbFriends'] = $rs.jStr(fbFriends);              
    };

    $rs.getUserId = function(){            
        return window.localStorage['userId'];  
    };
        
    $rs.setUserId = function(userId){          
        window.localStorage['userId'] = userId;              
    };

    $rs.getProfileFromWs = function(){     
        console.log('getProfileWsr');             
        return window.localStorage['profile'];  
    };
        
    /*
    ** @result = ws result
    */
    $rs.setProfileFromWs = function(getProfileWsr){    
        //console.log('setProfileFromWs:getProfileWsr='+$rs.jStr(getProfileWsr));      
        window.localStorage['profile'] = $rs.jStr(getProfileWsr);       

        //Need to set first time tood
        $rs.profile = [];
        $rs.profile.name = getProfileWsr.data.Name;
        $rs.profile.points = getProfileWsr.data.GiveBoostPoint + getProfileWsr.data.GetBoostPoint;
        $rs.profile.level = getProfileWsr.data.Level;
        $rs.profile.getBoostPt=getProfileWsr.data.GetBoostPoint;
        $rs.profile.giveBoostPt=getProfileWsr.data.GiveBoostPoint;
        $rs.profile.pic = getProfileWsr.data.PicUrl;

        $rs.setMixpanelProfile(getProfileWsr);

    };

    $rs.setMixpanelProfile=function(data)
    {
        //alert("setMixpanelProfile:$rs.getDevicePlatform()="+$rs.getDevicePlatform());
        mixpanel.people.set({
            "points": $rootScope.profile.points,
            "level": $rootScope.profile.level,
            "getBoostPt": $rootScope.profile.getBoostPt,
            "giveBoostPt": $rootScope.profile.giveBoostPt,
            "pic": $rootScope.profile.pic,
            "device": JSON.stringify($rs.getDevicePlatform()),
        });

        mixpanel.identify(data.id);
        mixpanel.alias(data.id);

        mixpanel.register_once({
            "referred_id": 123,
            "UserId": data.UserId,

        });

        mixpanel.register({
            "fb_data": data,
        });
    }

    $rs.setFbLoginResponse = function(res)
    {
        console.log('setFbLoginResponse:res='+$rs.jStr(res));
        //var fbId = res.authResponse.userID;
        var fbId = res.authResponse.id;
        console.log('setFbLoginResponse:fbId='+fbId);
        $rs.setFBUserId(fbId);
        $rs.fbLr = res;
    }

    $rs.setDeviceInfo = function(myDevice) 
    {
        $rs.setDevicePlatform(myDevice.platform);
    };

    $rs.getDeviceId = function(){         
        var deviceId = window.localStorage['did']; 
        console.log('getDeviceId:deviceId='+deviceId);
        return deviceId;
    };
        
    $rs.setDeviceId = function(deviceId){          
        $rs.myDeviceId = deviceId;     
        var before = window.localStorage['did'];    
        window.localStorage['did'] = deviceId;
        // If we just acquired a deviceId, where it was null before
        //if(!$rs.varIsSet(before) && $rs.varIsSet(deviceId)) 
        //{
            console.log('setDeviceId:updateDeviceId');
            $rs.updateDeviceId();                     
        //}
        console.log('setDeviceId:deviceId='+deviceId);
    };

    $rs.jStr = function(jsonObj)
    {
        return JSON.stringify(jsonObj, null, "\t");
    }

    $rs.PEDNING="PENDING";
    $rs.REQUEST="REQUEST";
    $rs.CIMPLITED="CIMPLITED"
});