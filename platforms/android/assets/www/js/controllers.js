angular.module('boosterapp.controllers', [])

        .controller('MainCtrl', function($scope, $rootScope, $ionicModal, $ionicLoading, PushNotifications) {

            var $rs = $rootScope;

            mixpanel.track('App_Started:MainCtrl');

            //TODO: check if removing is ok
            //PushNotifications.initialize();

            $rootScope.profile = {};
            $rootScope.profile.name = "";
            $rootScope.profile.pic = "";
            $rootScope.profile.points = "";
            $rootScope.profile.level = "";
            $rootScope.profile.getBoostPt = "";
            $rootScope.profile.giveBoostPt = "";

            $rootScope.fbUserId = "";

            //Get notfication
            $rootScope.notef = {};
            $rootScope.notef.count = 0;
            $rootScope.notef.list = [];

            $rootScope.showLoading = function() {
                $ionicLoading.show({
                    template: 'Please wait...'
                });
            };

            $rootScope.hideLoading = function() {
                $ionicLoading.hide();
            };

            $rootScope.getNotificationCount = function() {
                return ($rootScope.notef.count == 0 || $rootScope.notef.count == "") ? "0" : $rootScope.notef.count + "";
            };

            /*
             $rs.showModal=function(mod, pic, anim)
             {
             $ionicModal.fromTemplateUrl(mod, {
             scope: $rs,
             animation: anim
             }).then(function(modal) {
             $rs.modal = modal;
             });
             
             //Cleanup the modal when we're done with it!
             $rs.$on('$destroy', function() {
             $rs.modal.hide();
             //$rs.modal.remove();
             });
             // Execute action on hide modal
             $rs.$on('modal.hide', function() {
             // Execute action
             });
             // Execute action on remove modal
             $rs.$on('modal.removed', function() {
             // Execute action
             });
             $rs.$on('modal.shown', function() {
             if($rs.debug>0) console.log('Modal is shown!');
             });
             
             $rs.imageSrc = pic;
             
             $rs.modal.show();
             }
             */

            $rs.showTuts = function(b)
            {
                window.localStorage['isTutUpcoming1'] = b;
                window.localStorage['isTutUpcoming2'] = false;
                window.localStorage['didShowUpcoming2'] = false;
                window.localStorage['isTutRequests'] = b;
                window.localStorage['isTutGetBoost'] = b;
                window.localStorage['isTutChooseBooster'] = b;
                window.localStorage['isTutCompleted'] = b;
                window.localStorage['isShowTuts'] = false;
                $scope.isShowTutsAgain = false;

                $rs.isTutUpcoming1 = window.localStorage['isTutUpcoming1'];
                $rs.isTutUpcoming2 = window.localStorage['isTutUpcoming2'];
                $rs.isTutRequests = window.localStorage['isTutRequests'];
                $rs.isTutGetBoost = window.localStorage['isTutGetBoost'];
                $rs.isTutChooseBooster = window.localStorage['isTutChooseBooster'];
                $rs.isTutCompleted = window.localStorage['isTutCompleted'];

            }

        })

        .controller('LoadingCtrl', function($scope, $state, $rootScope, $ionicPlatform, Booster, Utility, $ionicSlideBoxDelegate, PushNotifications) {

            //alert('android working in LoadingCtrl!')
            var doAutoLogin = false;
            //if($rs.debug>0) console.log('isFirstEntrance='+window.localStorage['isFirstEntrance']);
            if (!$rs.varIsSet(window.localStorage['isFirstEntrance']))
            {
                window.localStorage['isFirstEntrance'] = true;
                $rs.isFirstEntrance = true;
            }
            if (window.localStorage['isFirstEntrance'] == "false")
            {
                doAutoLogin = true;
            }
            if (doAutoLogin == true)
            {
                $rootScope.doPromiseStartAppOnce();//.then(function(result) 
            }
            else
            {
                $state.go('app.login');
            }

        })

        .controller('LoginCtrl', function($scope, $state, $rootScope, $ionicPlatform, Booster, Utility, $ionicSlideBoxDelegate, PushNotifications) {

            var $rs = $rootScope;

            $scope.facebookLogin = function()
            {
                //if($rs.debug>0) console.log('facebookLogin');
                $rootScope.alreadyStarting = false;
                $rootScope.doPromiseStartAppOnce();
            }

            //Handle the slide
            $scope.nextSlide = function() {
                $rootScope.trackEvent("nextSlide");
                $rootScope.trackEvent("intro_screen_num=" + $ionicSlideBoxDelegate.slidesCount());
                if ($ionicSlideBoxDelegate.currentIndex() == $ionicSlideBoxDelegate.slidesCount() - 1) {
                    //$ionicSlideBoxDelegate.slide(0);  
                    $rs.alreadyStarting = false;
                    $rootScope.doPromiseStartAppOnce();
                    //$rootScope.impl_facebookLogin();
                }
                else {
                    $ionicSlideBoxDelegate.next();
                }
            };

            $scope.$on('$viewContentLoaded', function()
            {
                //$rootScope.doPromiseStartAppOnce();
            });

        })

        .controller('ProfileCtrl', function($scope, $rootScope) {
            $rs = $rootScope;
            $rs.promiseGetProfile();
        })

        .controller('MenuCtrl', function($scope, $rootScope, Utility) {
            $rs = $rootScope;

            $scope.share = function() {
                $rs.share();
            }

            $rs.share = function() {
                $rootScope.trackEvent('MenuCtrl:share');
                var message = "I'm using Booster App. Download it here!\n\nApp Store:\nhttps://itunes.apple.com/il/app/booster-app/id920384966?mt=8&uo=4\n\nGoogle play:\nhttps://play.google.com/store/apps/details?id=com.theboosterapp.booster\n\nGet ready to boost your life!!",
                        subject = 'Booster App',
                        fileOrFileArray = null,
                        url = null;

                var sharing = window.plugins.socialsharing;
                sharing.share(
                        message,
                        subject,
                        fileOrFileArray,
                        url,
                        function(result) {
                            $rootScope.trackEvent('socialsharing');
                            if ($rs.debug > 0)
                                console.log('Sharing success: ' + JSON.stringify(result));
                        },
                        function(result) {
                            $rootScope.trackError(error, "socialsharing");
                            if ($rs.debug > 0)
                                console.log('Sharing error: ' + JSON.stringify(result));
                        });
            }

        })

        .controller('UpcomingCtrl', function(Booster, $ionicModal, $scope, $state, $rootScope, $cordovaStatusbar, Utility) {
            var $rs = $rootScope;

            //if($rs.debug>0) console.log('UpcomingCtrl');
            if ($rs.debug > 0)
                console.log('UpcomingCtrl');

            if ($rs.isFirstEntrance)
            {
                if ($rs.debug > 0)
                    console.log('isFirstEntrance');
                $rs.isFirstEntrance = false;
                //$state.go($rs.pushState);     
            } else {
            }

            $rs.showPokeOption = function(buttonIndex)
            {
                if (buttonIndex == 2) // if CANCEL, return
                {
                    $rs.trackEvent('poke:cancel');
                    return;
                }
                $rs.trackEvent('poke:accept');
                //if($rs.debug>0) console.log('$rs.itemToPoke='+JSON.stringify($rs.itemToPoke));
                Booster.sendPoke($rs.itemToPoke.BoostId);
            }

            $scope.sendPoke = function(item)
            {
                //GABN: TODO: REMOVE after testing
                /*
                 var targetId = 521;
                 alert('test go to targetId='+targetId);
                 $rs.pushState = 'app.review';
                 $rs.pushTarget= targetId;
                 $state.go($rs.pushState, {boostId: $rs.pushTarget});
                 return;
                 */

                var date = new Date(item.EventDate);
                //if($rs.debug>0) console.log('sendPoke:item='+JSON.stringify(item));
                //if($rs.debug>0) console.log('sendPoke:date='+JSON.stringify(date));
                if (item.Status != "A") {
                    //if($rs.debug>0) console.log('after you accept, you can poke')
                    navigator.notification.confirm("After you accept, you can poke", null, "Poke", ["Ok"])
                    return;
                }
                var ts = date.getTime();
                var now = new Date();
                var ts_now = now.getTime();
                //if($rs.debug>0) console.log('ts='+ts);
                //if($rs.debug>0) console.log('ts_now='+ts_now);
                if (ts < ts_now) {
                    //if($rs.debug>0) console.log('after time of event, you can poke')
                    //navigator.notification.confirm("After time of event, you can poke", null, "Poke", ["Ok"])
                    //return;
                }
                $rs.itemToPoke = item;
                navigator.notification.confirm("Send a Poke?", $rs.showPokeOption, "Poke", ["Ok", "Cancel"])
            }

            /*
             $scope.simClick = function () {
             if($rs.debug>0) console.log('showTutUpcoming1');
             //$timeout(function() {
             //if($rs.debug>0) console.log('showTutUpcoming1:simClick');
             angular.element('#addboostBtn').trigger('click');
             //}, 100);
             };
             */

            $scope.showTutUpcoming1 = function(b) {
                if ($rs.debug > 0)
                    console.log('UpcomingCtrl:showTutUpcoming1:$scope.showTut='.b);
                $rs.isTutUpcoming1 = b;
                $rs.isTutGetBoost = false; // don't show 2nd get boost till after choose booster
                window.localStorage['isTutUpcoming1'] = b;
                //$state.go('app.getboost');
                //$state.transitionTo("app.getboost");
                //$state.href("app.getboost");
                //$scope.simClick();
            }

            $scope.$on('$viewContentLoaded', function()
            {
                if ($rs.debug > 0)
                    console.log("UpcomingCtrl:$viewContentLoaded");

                $rs.isShowTutsAgain = window.localStorage['isShowTuts'];
                if ($rs.debug > 0)
                    console.log('viewContentLoaded:$rs.isShowTutsAgain=' + $rs.isShowTutsAgain);

                $rs.isTutUpcoming2 = window.localStorage['laterShowTutUpcoming2'];
                window.localStorage['laterShowTutUpcoming2'] = false;

                // $rs will change for next next time
                if (!$rs.varIsSet($rs.isShowTutsAgain) || $rs.isShowTutsAgain == "true")
                {
                    if ($rs.debug > 0)
                        console.log('run:call:showTuts:true');
                    $rs.showTuts(true);
                }
                if ($rs.debug > 0)
                    console.log('viewContentLoaded:$rs.isShowTutsAgain=' + $rs.isShowTutsAgain);
                //TODO:remove
                //$rs.showTuts(true);

            });

            $scope.showTutUpcoming2 = function(b) {
                //if($rs.debug>0) console.log('UpcomingCtrl:showTutUpcoming2:$scope.showTut='+b);
                window.localStorage['isTutUpcoming2'] = b;
                $rs.isTutUpcoming2 = b;
            }

            /*
             // TODO: put in isFirstEntrance
             $scope.$on('$viewContentLoaded', 
             function(event){ 
             $rs.showModal('templates/image-modal.html', 'img/Template/PNG/new_boost.png', 'slide-in-up')
             });
             $rs.doDelayed($rs.showModal, 1000, ['templates/image-modal.html', 'img/Template/PNG/new_boost.png', 'slide-in-up']);
             */

            $rootScope.trackEvent('UpcomingCtrl');

            //Get upcoming boost
            $rootScope.showLoading();

            //Update request boost count
            $rootScope.updateRequestCount();

            //$cordovaStatusbar.show();
            var userId = $rootScope.getUserId();
            if ($rs.debug > 0)
                console.log("upcoming:userid=" + userId);
            Booster.getUpcomingBoost(userId).then(function(result) {
                $rootScope.hideLoading();
                if ($rs.debug > 0)
                    console.log('UpcomingCtrl:getUpcomingBoost:$scope.list=' + $rs.jStr($scope.list));
                $scope.list = result.data;
            },
                    function(errorMessage) {
                        $rootScope.hideLoading()
                        $rootScope.trackError(errorMessage, "getUpcomingBoost");
                        if ($rs.debug > 0)
                            console.log('UpcomingCtrl:getUpcomingBoost:errorMessage=' + errorMessage);
                        if ($rs.debug > 0)
                            console.log("Error in getting upcoming list", "Error occured while getting upcoming list");
                    });

            $rootScope.updateNotifications();

        })

        .controller('RequestsCtrl', function($scope, $rootScope, $stateParams, $ionicPopup, Booster, Utility, $cordovaToast, $state) {
            var $rs = $rootScope;
            $rootScope.trackEvent('RequestsCtrl');

            $scope.showTutRequests = function(b) {
                if ($rs.debug > 0)
                    console.log('RequestsCtrl:showTutRequests:isTutRequests=' + b);
                window.localStorage['isTutRequests'] = b;
                $rs.isTutRequests = b;
            }

            //Update request boost count
            $rootScope.updateRequestCount();

            //Get upcoming boost  
            $rootScope.showLoading();
            Booster.getRequestBoost($rootScope.getUserId()).then(function(result) {
                $rootScope.hideLoading();
                $scope.list = result.data;
                $rootScope.requestCount = result.data.length;
            },
                    function(errorMessage) {
                        $rootScope.hideLoading()
                        $rootScope.trackError(errorMessage, "getRequestBoost");
                        if ($rs.debug > 0)
                            console.log("Error in request list", "Error occured while getting request list");
                    }
            );

            $scope.accept = function(id) {
                $ionicPopup.confirm({
                    title: 'Confirm',
                    content: 'Are you sure you want to accept?'
                }).then(function(res) {
                    if (res) {
                        $rootScope.showLoading();
                        //Accepted  
                        Booster.acceptBoostRequest(id).then(function(result) {
                            $rootScope.hideLoading()
                            //if($rs.debug>0) console.log("Info", "Requeset accepted successfully!");                     
                            $cordovaToast.showShortCenter('Request accepted successfully!').then(function(success) {
                                $state.go('app.upcoming');
                            }, function(error) {
                                // error
                                $rootScope.trackError(error, "showShortCenter:acceptBoostRequest");
                            });

                        },
                                function(errorMessage) {
                                    $rootScope.hideLoading()
                                    $rootScope.trackError(errorMessage, "acceptBoostRequest");
                                    if ($rs.debug > 0)
                                        console.log("Error", "Error occured while updateding Request");
                                }
                        );
                    }
                });
            }

            $scope.decline = function(id) {
                $ionicPopup.confirm({
                    title: 'Confirm',
                    content: 'Are you sure you want to decline?'
                }).then(function(res) {
                    if (res) {
                        $rootScope.showLoading();
                        //Decline                
                        Booster.declineBoostRequest(id).then(function(result) {
                            $rootScope.hideLoading()
                            $cordovaToast.showShortCenter('Requeset declined successfully!').then(function(success) {
                                $state.go('app.upcoming');
                            }, function(error) {
                                // error
                                $rootScope.trackError(error, "showShortCenter:decline");
                            });
                            //if($rs.debug>0) console.log("Info", "Requeset declined successfully!");
                        },
                                function(errorMessage) {
                                    $rootScope.hideLoading()
                                    if ($rs.debug > 0)
                                        console.log("Error", "Error occured while updating request");
                                    $rootScope.trackError(error, "declineBoostRequest");
                                }
                        );
                    }

                });
            }

        })

        .controller('CompletedCtrl', function($scope, $rootScope, $stateParams, Booster, $state, Utility) {
            var $rs = $rootScope;
            $rootScope.trackEvent('CompletedCtrl');
            //Get completed list

            $scope.showTutCompleted = function(b) {
                if ($rs.debug > 0)
                    console.log('CompletedCtrl:showTutCompleted:isTutCompleted=' + b);
                $rs.isTutCompleted = b;
                window.localStorage['isTutCompleted'] = b;
            }

            $rootScope.showLoading();
            Booster.getCompletedBoost($rootScope.getUserId()).then(function(result) {
                $rootScope.hideLoading();
                $scope.list = result.data;
            },
                    function(errorMessage) {
                        $rootScope.hideLoading()
                        $rootScope.trackError(errorMessage, "getCompletedBoost");
                        if ($rs.debug > 0)
                            console.log("Error", "Error occured while getting completed list");
                    }
            );
        })

        .controller('GetBoostCtrl', function($scope, $rootScope, $stateParams, Booster, Utility, $cordovaToast, $state) {
            var $rs = $rootScope;
            if ($rs.debug > 0)
                console.log('GetBoostCtrl');
            $rootScope.trackEvent('GetBoostCtrl');
            var pic = "https://graph.facebook.com/" + $stateParams.fbid + "/picture?type=square&height=200&width=200";
            $scope.boost = {};
            $scope.boost.toFbId = $stateParams.fbid;
            $scope.boost.picUrl = (($stateParams.fbid + "") == "0" ? "img/addpic.png" : pic);
            $scope.boost.comment = "";
            $scope.boost.evtDateTime = "Choose Date + Time";
            $scope.boost.evtTime = "";
            $scope.boost.after = "";

            $scope.showTutGetBoost = function(b) {
                event.preventDefault();
                event.stopPropagation();
                if ($rs.debug > 0)
                    console.log('GetBoostCtrl:showTutGetBoost:isTutGetBoost=' + b);
                $rs.isTutGetBoost = b;
                window.localStorage['isTutGetBoost'] = b;
            }

            $scope.showDatetime = function()
            {
                if ($rs.devicePlatform == "iOS") {
                    //if($rs.debug>0) console.log('showDatetime');
                    var options = {
                        date: new Date(),
                        mode: 'datetime',
                        allowOldDates: false
                    };

                    datePicker.show(options, function(dto) {
                        var sdatetime = dto.format('yyyy-mm-dd HH:MM');
                        if ($rs.debug > 0)
                            console.log('showDatetime:show:sdatetime=' + sdatetime);
                        $scope.boost.evtDateTime = sdatetime;
                        $scope.$apply();
                    });
                }
                else { //if($rs.devicePlatform == "Android") {
                    //if($rs.debug>0) console.log('showDatetime');
                    var options = {
                        date: new Date(),
                        mode: 'date',
                        allowOldDates: false
                    };
                    datePicker.show(options, function(dto) {
                        var sdate = dto.format('yyyy-mm-dd ');
                        if ($rs.debug > 0)
                            console.log('showDatetime:show:sdate=' + sdate);
                        $scope.boost.evtDateTime = sdate;
                        //$scope.$apply();

                        var optionsTime = {
                            date: new Date(),
                            mode: 'time',
                            allowOldDates: true
                        };
                        datePicker.show(optionsTime, function(dto2) {
                            var stime = dto2.format('HH:MM');
                            if ($rs.debug > 0)
                                console.log('showDatetime:show:stim=' + stime);
                            $scope.boost.evtDateTime += stime;
                            $scope.$apply();
                        });
                    });
                }
            }

            $scope.addBoost = function() {
                //Check validation
                var error = "";
                if ($scope.boost.toFbId == 0 || $scope.boost.toFbId == "" || $scope.boost.toFbId == null || $scope.boost.toFbId + "" == "undefined") {
                    error = "Please choose a booster";
                }
                else if ($scope.boost.comment == "") {
                    error = "Please enter title";
                }
                else if ($scope.boost.evtDateTime == "") {
                    error = "Please enter date";
                }
                else if ($scope.boost.evtTime == "") {
                    //error = "Please enter time"; 
                }
                else if ($scope.boost.after == "") {
                    //error = "Please selecct alert"; 
                }

                if (error != "") {
                    $cordovaToast.showShortCenter(error).then(function(success) {
                    }, function(error) {
                        // error
                        $rootScope.trackUserError(errorMessage, "addBoost");
                    });
                    return false;
                }

                var offset = new Date().getTimezoneOffset();
                //if($rs.debug>0) console.log('offset='+offset);

                $rootScope.showLoading();

                //var eDateTimezone = $scope.boost.evtDateTime + " " + $scope.boost.evtTime + " " + offset;
                //var eDateTimezone = $scope.boost.evtDateTime + " " + offset;
                //if($rs.debug>0) console.log('eDateTimezone='+eDateTimezone);

                ////if($rs.debug>0) console.log(eDateTimezone);

                Booster.addBoost($rootScope.getUserId(), $scope.boost.toFbId, $scope.boost.evtDateTime, offset, $scope.boost.comment, $scope.boost.after).then(function(result) {
                    $rootScope.hideLoading();
                    $cordovaToast.showShortCenter('Boost Added Successfully!').then(function(success) {
                        if (!$rs.varIsSet(window.localStorage['didShowUpcoming2']) || window.localStorage['didShowUpcoming2'] == "true")
                        {
                            window.localStorage['laterShowTutUpcoming2'] = false;
                        } else {
                            window.localStorage['laterShowTutUpcoming2'] = true;
                            window.localStorage['didShowUpcoming2'] = true;
                        }
                        $state.go('app.upcoming');
                    }, function(error) {
                        // error
                        $rootScope.trackError(error, "addBoost");
                    });
                    //if($rs.debug>0) console.log("Info", "Boost created successfully");
                },
                        function(errorMessage) {
                            $rootScope.hideLoading()
                            $rootScope.trackError(errorMessage, "addBoost");
                            if ($rs.debug > 0)
                                console.log("Error", "Error occured while adding boost");
                        }

                );
            }
        })

        .controller('NotificationCtrl', function($location, $state, $scope, $rootScope, Booster, Utility) {
            var $rs = $rootScope;
            $rootScope.trackEvent('NotificationCtrl');
            $rootScope.showLoading();
            $scope.result;

            Booster.markNotfRead($rootScope.getUserId()).then(function(result) {
                //if($rs.debug>0) console.log('markNotfRead:result='+JSON.stringify(result));
                $rootScope.hideLoading();
                $rootScope.notef.count = 0;
                $scope.result = result;
            },
                    function(errorMessage) {
                        $rootScope.hideLoading()
                        $rootScope.trackError(errorMessage, "NotificationCtrl:markNotfRead");
                        if ($rs.debug > 0)
                            console.log("Error", "Error occured while NotificationCtrl, adding boost");
                    });

            $scope.getClass = function(index) {
                var item = $rootScope.notef.list[index];
                //if($rs.debug>0) console.log('NotificationCtrl:getClass:item.Type='+item.Type);
                var classes = {};
                if (item.IsRead)
                {
                    classes.oldNotif = true;
                }
                else
                {
                    classes.newNotif = true;
                }
                classes[item.Type] = true;
                //if($rs.debug>0) console.log('classes='+JSON.stringify(classes));
                return classes;
            }

            $scope.showDetail = function(item) {
                if (item.Type == "AcceptBoost") {
                    $state.go("app.upcoming");
                }
                else if (item.Type == "RejectBoost") {
                    $state.go("app.upcoming");
                }
                else if (item.Type == "Selfie") {
                    $location.path("/app/review/" + item.TargetId + "/true");
                }
                else if (item.Type == "BoostRequest") {
                    $state.go("app.requests");
                }
                else if (item.Type == "Review") {
                    $location.path("/app/creview/" + item.TargetId + "/false");
                }
                else if (item.Type == "UpdateBoost") {
                    $state.go("app.upcoming");
                }

            }
        })

        .controller('ChooseBoostCtrl', function($scope, $rootScope, $q, $location, Booster, Utility) {
            var $rs = $rootScope;
            $rootScope.trackEvent("ChooseBoostCtrl");
            $rootScope.showLoading();
            $scope.boosters = [];
            $scope.$on('$viewContentLoaded', function(event) {
                tryGetFacebookFriends();
            });

            $rs.showTutChooseBooster = function(b) {
                if ($rs.debug > 0)
                    console.log('ChooseBoostCtrl:$scope.showTutChooseBooster=' + $scope.showTut);
                $rs.isTutChooseBooster = b;
                window.localStorage['isTutChooseBooster'] = b;
                $rs.isTutGetBoost = true;
            }

            $scope.invite = function() {
                $rs.share();
            }

            $scope.selectBoost = function(booster) {
                $location.path("/app/getboost/" + booster.id)
            }


            var tryGetFacebookFriends = function() {
                //Check if connected
                $rs.getFriends();
                /*
                 facebookConnectPlugin.getLoginStatus(
                 function(res){
                 if(res.status + "" == "connected"){ 
                 getFriends();    
                 }
                 else{
                 //TODO: Login
                 //Get frined
                 }      
                 },
                 function(err){
                 if($rs.debug>0) console.log("err : " + err);
                 }
                 );
                 */
            }

            //Merge with server default booster
            var addDefaultBooster = function(boosters) {
                $rootScope.trackEvent("addDefaultBooster");

                var currentFbIb = $rootScope.getFBUserId();

                Booster.getDefaultBoosters().then(function(result) {
                    // TODO: change 90 to server side behavior to always return the def boosters if it is not
                    for (var i = 0; i < result.data.length && boosters.length < 90; i++)
                    {
                        //Check if already exist in list
                        var isExist = false;
                        for (var x = 0; x < boosters.length; x++)
                        {
                            if (boosters[x].id == result.data[i].FbUserId || result.data[i].FbUserId == currentFbIb)
                            {
                                isExist = true;
                                break
                            }
                        }

                        if (!isExist)
                        {
                            var item =
                                    {
                                        "name": result.data[i].Name,
                                        "picUrl": result.data[i].PicUrl,
                                        "id": result.data[i].FbUserId
                                    }

                            boosters.unshift(item);
                        }
                    }

                    //$scope.$apply(function() {     
                    //  $scope.boosters = boosters;
                    //});
                },
                        function(errorMessage) {
                            $rootScope.trackError(errorMessage, "getDefaultBoosters");
                            if ($rs.debug > 0)
                                console.log("Error", "Error occured while getting boost details for id : " + $stateParams.boostId);
                        });

            }

            $rs.getFriends = function() {
                var d = $q.defer();
                //if($rs.getDevicePlatform()=="Android")
                facebookConnectPlugin.api($rootScope.getFBUserId() + "/friends?fields=id,name,picture", ["user_friends"],
                        //facebookConnectPlugin.makeGraphCall($rootScope.getFBUserId() + "/friends?fields=id,name,picture", ["user_friends"],
                                function(result) {

                                    //if($rs.debug>0) console.log('getFriends');
                                    $rootScope.trackEvent("getFriends");
                                    response = result;
                                    var boosters = [];
                                    var boosterFbIds = [];

                                    if (response.data == null || response.data == undefined || response.data.length <= 0)
                                    {
                                        addDefaultBooster(boosters);
                                    }
                                    else
                                    {
                                        for (var i = 0; i < response.data.length; i++)
                                        {
                                            var item =
                                                    {
                                                        "name": response.data[i].name,
                                                        "picUrl": response.data[i].picture.data.url,
                                                        "id": response.data[i].id
                                                    }

                                            boosters.push(item);
                                            boosterFbIds.push(item.id);
                                        }
                                    }

                                    $scope.boosters = boosters;//response.data;   
                                    if (boosters.length < 90 || $rs.isTutCompleted)
                                        addDefaultBooster(boosters);

                                    $rootScope.hideLoading();

                                    d.resolve(boosterFbIds);

                                },
                                function(error) {
                                    if ($rs.debug > 0)
                                        console.log("Failed: " + error);
                                    d.reject(error);
                                });
                        return d.promise;
                    };
        })

                .controller('SelfieBoostCtrl', function($scope, $rootScope, $stateParams, Booster, Utility) {
                    $rootScope.trackEvent("SelfieBoostCtrl");

                    $rootScope.showLoading();

                    Booster.getBoostDetails($stateParams.boostId).then(function(result) {
                        $rootScope.hideLoading();
                        if ($rs.debug > 0)
                            console.log("Booster.getBoostDetails:$rootScope.details=" + JSON.stringify($rootScope.details));
                        $rootScope.details = result.data;
                        $scope.details = result.data;

                    },
                            function(errorMessage) {
                                $rootScope.hideLoading()
                                $rootScope.trackError(errorMessage, "getBoostDetails");
                                if ($rs.debug > 0)
                                    console.log("Error", "Error occured while getting boost details for id : " + $stateParams.boostId);
                            });

                })

                .controller('ReviewCtrl', function($scope, $state, Booster, $rootScope, $stateParams, $cordovaToast, Booster, Utility) {
                    $rootScope.trackEvent("ReviewCtrl");
                    $rs = $rootScope;

                    $scope.selfie = {};
                    $scope.selfie.selfieId = "";

                    var uid = $rs.getUserId();
                    if (uid == 1640 || uid == 1681 || uid == 1641)
                    {
                        //alert('ReviewCtrl:$stateParams='+JSON.stringify($stateParams));
                    }
                    console.log('ReviewCtrl:$stateParams=' + JSON.stringify($stateParams));

                    $rootScope.showLoading();
                    Booster.getSelfie($stateParams.boostId).then(function(result) {
                        $rootScope.hideLoading();
                        $scope.selfie = result.data;

                        if ($rs.getUserId() == $scope.selfie.UserId)
                        {
                            $scope.selfie.iAmBooster = 'true';
                        }

                        $scope.selfie.selfieId = result.data.SelfieId;
                        $scope.selfie.hasAction = $stateParams.hasAction;

                        $scope.rcFbShare = function()
                        {
                            if ($rs.debug > 0)
                                console.log("rcFbShare:$scope.selfie.UserId=" + JSON.stringify($scope.selfie.UserId));
                            var selfie = $scope.selfie;
                            //selfie.ImageData = selfie.image;
                            selfie.message = "@" + selfie.RequestUserName + " boosted me to " + selfie.BoostTitle;
                            $rs.fbShare(selfie);
                        }
                    },
                            function(errorMessage) {
                                $rootScope.hideLoading()
                                $rootScope.trackError(errorMessage, "getSelfie");
                                if ($rs.debug > 0)
                                    console.log("Error", "Error occured while gettting selfie for id:" + $stateParams.boostId);
                            });

                    $scope.review = {};
                    $scope.review.comment = "";

                    $scope.sendReview = function(isLovedIt) {
                        $rootScope.showLoading();
                        Booster.addBoostReview($scope.selfie.selfieId, $scope.review.comment, isLovedIt).then(function(result) {
                            $state.go('app.completed');
                            $cordovaToast.showShortCenter('Review sent successfully!').then(function(success) {
                            }, function(error) {
                                // error
                                $rootScope.trackError(error, "addBoostReview");
                            });
                        },
                                function(errorMessage) {
                                    $rootScope.hideLoading()
                                    $rootScope.trackError(errorMessage, "addBoostReview");
                                    if ($rs.debug > 0)
                                        console.log("Error", "Error occured while sending review for selfie id:" + $scope.selfie.selfieId);
                                });
                    }

                    $scope.back = function() {
                        $state.go('app.completed');
                    }

                })

                .controller('CompletedReviewCtrl', function($scope, $state, $stateParams, $rootScope, Booster, Utility, $cordovaToast) {
                    $rs = $rootScope;
                    $rootScope.trackEvent("CompletedReviewCtrl");
                    $rootScope.showLoading();
                    Booster.getSelfie($stateParams.boostId).then(function(result) {
                        $rootScope.hideLoading();
                        $rootScope.details = result.data;
                        $scope.selfie = result.data;
                        $scope.selfie.isMySelfie = false;
                        if ($rs.debug > 0)
                            console.log('CompletedReviewCtrl:getSelfie:$scope.selfie.isMySelfie=' + $scope.selfie.isMySelfie);
                        if ($rs.getUserId() == result.data.UserId)
                        {
                            $scope.selfie.isMySelfie = 'true';
                        }
                        if ($rs.debug > 0)
                            console.log('CompletedReviewCtrl:$scope.selfie.isMySelfie=' + $scope.selfie.isMySelfie);

                        $scope.crcfbShare = function()
                        {
                            if ($rs.debug > 0)
                                console.log('CompletedReviewCtrl:sendSelfie:fbShare:$scope.selfie.UserId' + $scope.selfie.UserId);
                            var selfie = $scope.selfie;
                            //selfie.ImageData = selfie.image;
                            selfie.message = "@" + $rs.details.RequestUserName + " boosted me to " + $rs.details.Text;
                            //selfie.message = "@"+$rs.details.RequestUserName + " boosted me to " + $rs.details.Text;
                            $rootScope.fbShare(selfie);
                        }

                    },
                            function(errorMessage) {
                                $rootScope.hideLoading()
                                $rootScope.trackError(errorMessage, "getSelfie");
                                if ($rs.debug > 0)
                                    console.log("Error", "Error occured while gettting selfie for id:" + $stateParams.selfieId);
                            });

                    $scope.done = function() {
                        $state.go('app.completed');
                    }

                })

                .controller('TakeSelfieBoostCtrl', function($scope, $rootScope, $state, $cordovaCamera, $cordovaToast, Booster, Utility) {
                    $rootScope.trackEvent("TakeSelfieBoostCtrl");
                    $rs = $rootScope;
                    /*
                     $scope.sendAndShareSelfie = function(){
                     $scope.sendSelfie();
                     //TODO: call as thenable
                     $scope.fbShare();
                     }
                     */

                    /*
                     $rs.doImageResize = function(imageData)
                     {
                     alert('doImageResizeimageData='+imageData);
                     window.imageResizer.resizeImage(
                     function(data) { 
                     var image = new Image();//document.getElementById('myImage');
                     image.src = "data:image/png;base64," + imageData;//data.imageData; 
                     if($rs.debug>4) console.log('image.src='+image.src);
                     if($rs.debug>4) console.log('image.src.length='+image.src.length);
                     if($rs.debug>4) alert('image.src='+image.src);
                     if($rs.debug>4) alert('image.src.length='+image.src.length);
                     $scope.selfie.image = image.src;
                     }, function (error) {
                     console.log("Error : \r\n" + error);
                     }, imageDataInBase64, 0.5, 0.5, {resizeType:ImageResizer.RESIZE_TYPE_FACTOR ,format:'png'});
                     }
                     */

                    $scope.sendSelfie = function(doShare) {
                        $rootScope.trackEvent("sendSelfie");
                        $rootScope.showLoading();
                        Booster.sendSelfie($rootScope.details.BoostId, $scope.selfie.comment, $scope.selfie.image).then(function(result) {
                            $rootScope.hideLoading();
                            $cordovaToast.showShortCenter('Selfie sent successfully!').then(function(success) {
                                if (doShare == true) {
                                    if ($rs.debug > 0)
                                        console.log('TakeSelfieBoostCtrl:sendSelfie:fbShare:$scope.selfie.UserId' + $scope.selfie.UserId);
                                    var selfie = $scope.selfie;
                                    selfie.ImageData = selfie.image;
                                    $scope.details = $rs.details;
                                    selfie.message = "@" + $rs.details.RequestUserName + " boosted me to " + $rs.details.Text;
                                    $rs.fbShare(selfie).then(function(result) {
                                        $state.go('app.completed');
                                    });
                                }
                                $state.go('app.completed');
                            }, function(error) {
                                // error
                                $rootScope.trackError(error, "showShortCenter:send_selfie");
                            });
                        },
                                function(errorMessage) {
                                    $rootScope.hideLoading()
                                    $rootScope.trackError(errorMessage, "sendSelfie");
                                    if ($rs.debug > 0)
                                        console.log("Error", "Error occured when sending selfie");
                                });

                    }

                    $scope.selfie = {};
                    $scope.selfie.comment = "";
                    $scope.selfie.image = "";

                    var options = {
                        quality: 100,
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: Camera.PictureSourceType.CAMERA,
                        targetWidth: 1000,
                        targetHeight: 1000,
                        encodingType: Camera.EncodingType.JPEG,
                        mediaType: 0,
                        allowEdit: true,
                        correctOrientation: true,
                        saveToPhotoAlbum: false,
                        popoverOptions: CameraPopoverOptions,
                        cameraDirection: 1,
                    };

//                    alert('options=' + JSON.stringify(options));

//                    alert('getPicture:$rs.getDevicePlatform()=' + $rs.getDevicePlatform());

                    $cordovaCamera.getPicture(options).then(function(imageData) {
                        //if($rs.debug>0) console.log(JSON.stringify(imageData));

                        //if($rs.devicePlatform=="Android")
                        //{
                        // if($rs.debug>=3)console.log('2 call doCrop on imageData='+imageData);
                        // if($rs.debug>=1)console.log('2 call doCrop on imageData.length='+imageData.length);
                        // if($rs.debug>=9)alert('2 call doCrop on imageData='+imageData);
                        // if($rs.debug>=7)alert('2 call doCrop on imageData.length='+imageData.length);
                        //$scope.selfie.image = imageData;
                        alert('getPicture:imageData=' + imageData);
                        $rs.addImageToCanvas(imageData);
                        //imageData = $rs.doCrop(imageData);

                        //$rs.doImageResize(imageData);
                        //$rs.startCropping(imageUrl);
                        // if($rs.debug>=7)alert('2 after doCrop, imageData.length='+imageData.length);
                        //$scope.selfie.image = imageData;
                        /*
                         }  
                         else 
                         {
                         $scope.selfie.image=imageData;
                         }
                         */

                        // Success! Image data is here
                    }, function(err) {
                        // An error occured. Show a message to the user
                        $rootScope.trackError(err, "getPicture");
                    });

                    $rs.addImageToCanvas = function(imageUri) {
//                        alert('addImageToCanvas:imageUri=' + imageUri);
//                        var myCanvas = document.getElementById('croppedImage');
//                        var ctx = myCanvas.getContext('2d');
//                        var img = new Image;
//                        img.onload = function() {
//                            alert('addImageToCanvas:img.onload');
//                            ctx.drawImage(img, 0, 0, 160, 160, 0, 0, 160, 75); // Or at whatever offset you like
//                            alert('addImageToCanvas:img.onload:2');
//                            var imageBase64 = myCanvas.toDataURL();
//                            alert('imageBase64=' + imageBase64);
//                        };
//                        img.src = imageUri;


                        var canvas = document.getElementById("croppedImage");
                        var ctx = canvas.getContext("2d");

                        img = new Image();
                        var screenSize = screen.width;
                        img.onload = function() {
                            canvas.width = screenSize;
                            canvas.height = screenSize;
                            ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, screenSize, screenSize);
                        }
                        img.src = imageUri;
                        var imageBase64 = canvas.toDataURL();
                        console.log("Before | " + imageBase64);
                        imageBase64 = imageBase64.replace("data:image/png;base64,", "");
                        console.log("After | " + imageBase64);
                        $scope.selfie.image = imageBase64;
                    }

                    /*
                     $rs.startCropping = function(imageURI)
                     {
                     var $image = $("#cropImage"),
                     $dataX1 = $("#data-x1"),
                     $dataY1 = $("#data-y1"),
                     $dataX2 = $("#data-x2"),
                     $dataY2 = $("#data-y2"),
                     $dataHeight = $("#data-height"),
                     $dataWidth = $("#data-width");
                     //    alert(imageURI);
                     $image.attr("src", imageURI);
                     
                     $image.cropper({
                     aspectRatio: 1,
                     preview: ".img-preview",
                     done: function(data) {
                     $dataX1.val(data.x1);
                     $dataY1.val(data.y1);
                     $dataX2.val(data.x2);
                     $dataY2.val(data.y2);
                     $dataHeight.val(data.height);
                     $dataWidth.val(data.width);
                     avatarSrcDataJson = JSON.stringify(data);
                     $("#avatar_src").val(avatarSrcDataJson);
                     }
                     });
                     $("#disable").click(function() {
                     $image.cropper("disable");
                     });
                     $("#free-ratio").click(function() {
                     $image.cropper("setAspectRatio", "auto");
                     });
                     
                     var $dragmove = $("#drag-move");
                     
                     $image.on("dragmove", function() {
                     $dragmove.addClass("btn-info").siblings().removeClass("btn-info");
                     });
                     }
                     */

                    /*
                     $rs.doCrop = function(imageData) {
                     if($rs.debug>=7)alert('2 in doCrop');
                     
                     var canvas = document.createElement('canvas'),
                     ctx = canvas.getContext('2d');
                     
                     canvas.width = 100;
                     canvas.height = 100;
                     
                     //ctx.putImageData = imageData;
                     var cropper = new Image();
                     //cropper.putImageData = imageData;
                     cropper.src = 'data:image/png;base64,'+imageData;
                     
                     if($rs.debug>=2)alert('cropper='+cropper);
                     
                     // crop.x,crop.y,crop.w,crop.y,draw.x,draw.y,draw.w,draw.h
                     ctx.drawImage(cropper, 0, 0, 100, 100, 0, 0, 100, 100);
                     
                     var cropped = canvas.toDataURL('data:image/png');
                     //cropped = cropped.split(",");
                     //cropped = cropped[1];
                     
                     if($rs.debug>=2)console.log('2 in doCrop:cropped='+cropped);
                     if($rs.debug>=9)alert('3 return doCrop:cropped='+cropped);
                     if($rs.debug>=1)console.log('2 in doCrop:cropped.length='+cropped.length);
                     if($rs.debug>=8)alert('3 return doCrop:cropped.length='+cropped.length);
                     
                     $scope.selfie.image = cropped;
                     
                     var cropimg = document.createElement('img')
                     cropimg.src = cropped;
                     document.body.appendChild(cropimg);
                     
                     return cropped;
                     //var cropimg = document.createElement('img')
                     //cropimg.src = cropped;
                     //document.body.appendChild(cropimg)      
                     }
                     */

                    // code for customized camera plugin
                    // navigator.customCamera.getPicture('photo.png', function success(fileUri) {
                    // }, function failure(error) {
                    // }, {
                    //     quality: 80,
                    //     targetWidth: 120,
                    //     targetHeight: 120,
                    //     topText: 'Your booster: Justin Bieber'
                    // });

                })

                .controller('EditBoostCtrl', function($scope, $rootScope, $ionicSideMenuDelegate) {
                    $rootScope.trackEvent("EditBoostCtrl");
                })
