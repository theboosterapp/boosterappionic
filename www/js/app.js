angular.module('boosterapp', ['ionic', 'ngCordova', 'boosterapp.utility', 'boosterapp.services', 'boosterapp.controllers'])

.run(function($ionicPlatform, Utility, $state, $rootScope, PushNotifications) {

  var $rs = $rootScope;
  $rs.debug = 9;

  $ionicPlatform.ready(function() {

    if($rs.debug>0) console.log("$ionicPlatform.ready");
    //$rootScope.doOnReady();

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if($rs.debug>0) console.log("CONSOLE LOG WORKS AT APP.JS RUN: ionic.Platform.ready:ionic.Platform.device()="+ionic.Platform.device());

    $rs.boosterVersion="v1.5.8.a";
    var installed_version = window.localStorage['version'];
    if($rs.debug>0) console.log('installed_version='+installed_version);
    if(installed_version != $rs.boosterVersion)
    {
      if($rs.debug>0) console.log('$rs.boosterVersion='+$rs.boosterVersion);
      //if($rs.debug>0) console.log('window.localStorage.clear()');
      //window.localStorage.clear();
      window.localStorage['version'] = $rs.boosterVersion;
    }

    //navigator.splashscreen.show();

    //alert("$ionicPlatform.ready");

    $rs.setDeviceInfo(ionic.Platform.device());
    var theDp = $rs.getDevicePlatform();
    //alert('ionicPlatform.ready:theDp='+theDp);

    PushNotifications.initialize();

    /* MOVED TO LOADING CTRL
    var doAutoLogin = false;
    //alert('isFirstEntrance='+window.localStorage['isFirstEntrance']);
    if(!$rs.varIsSet(window.localStorage['isFirstEntrance']))
    {
      window.localStorage['isFirstEntrance'] = true;
      $rs.isFirstEntrance = true;      
    }
    if(window.localStorage['isFirstEntrance'] == "false")
    {
      doAutoLogin = true;
    }
    alert('doAutoLogin='+doAutoLogin);
    if(doAutoLogin == true)
    {
      $rootScope.doPromiseStartAppOnce();//.then(function(result) 
    }
    */

    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.cordova.logger) {
      window.cordova.logger.__onDeviceReady();
      if($rs.debug>0) console.log('onDeviceReady:window.facebookConnectPlugin='+window.facebookConnectPlugin);
    }
    if(window.StatusBar) {
      if($rs.getDevicePlatform() != "Android") 
        StatusBar.setStyleForStatusBar(1);
      StatusBar.hide();
    }     
    if(window.facebookConnectPlugin) {
    }

  });

  ionic.Platform.ready(function() {
    //alert("ionic.Platform.ready");
    //$rootScope.doOnReady();
  });

})

.config(function($stateProvider, $urlRouterProvider) {
  var baseUrl = "";//"http://lin-res.mapitpix.com/Booster/ionic_git/www/";
  $stateProvider


    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: baseUrl + "templates/menu.html",
      controller: 'MenuCtrl'
    })

    .state('app.loading', {
      url: "/loading",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/loading.html",
          controller: 'LoadingCtrl'
        }
      }
    })

    .state('app.login', {
      url: "/login",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/login.html",
          controller: 'LoginCtrl'
        }
      }
    })

    .state('app.choosebooster', {
      url: "/choosebooster",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/choosebooster.html",
          controller: 'ChooseBoostCtrl'
        }
      }
    })

    .state('app.selfie', {
      url: "/selfie/:boostId",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/selfie.html",
          controller: 'SelfieBoostCtrl'
        }
      }
    })

    .state('app.takeselfie', {
      url: "/takeselfie",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/takeselfie.html",
          controller: 'TakeSelfieBoostCtrl'
        }
      }
    })

    .state('app.editboost', {
      url: "/editboost",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/editboost.html",
          controller: 'EditBoostCtrl'
        }
      }
    })    

    .state('app.getboost', {
      url: "/getboost/:fbid",
      views: {
        'menuContent' : {
          templateUrl: baseUrl + "templates/getboost.html",
          controller: 'GetBoostCtrl'
        }
      }
    })
//          templateUrl: "http://res.theboosterapp.com/ionic_git/www/templates/upcoming.html",
    .state('app.upcoming', {
      url: "/upcoming",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/upcoming.html",
          controller: 'UpcomingCtrl'
        }
      }
    })

    .state('app.requests', {
      url: "/requests",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/requests.html",
          controller: 'RequestsCtrl'
        }
      }
    })

    .state('app.completed', {
      url: "/completed",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/completed.html",
          controller: 'CompletedCtrl'
        }
      }
    })

    .state('app.review', {
      url: "/review/:boostId/:hasAction",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/review.html",
          controller: 'ReviewCtrl'
        }
      }
    })

    .state('app.completedreview', {
      url: "/creview/:boostId/:hasAction",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/completedreview.html",
          controller: 'CompletedReviewCtrl'
        }
      }
    })

    .state('app.notification', {
      url: "/notification",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/notification.html",
          controller: 'NotificationCtrl'
        }
      }
    })

    .state('app.profile', {
      url: "/profile",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/profile.html",
          controller: 'ProfileCtrl'
        }
      }
    })
    
    .state('app.contact', {
      url: "/contact",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/contact.html"
        }
      }
    })

    .state('app.about', {
      url: "/about",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/about.html"
        }
      }
    })

    .state('app.intro1', {
      url: "/intro1",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/intro1.html"
        }
      }
    })

    .state('app.intro2', {
      url: "/intro2",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/intro2.html"
        }
      }
    })

    .state('app.intro3', {
      url: "/intro3",
      views: {
        'menuContent' :{
          templateUrl: baseUrl + "templates/intro3.html"
        }
      }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/loading');
});
