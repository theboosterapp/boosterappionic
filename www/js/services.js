'use strict';

angular.module('boosterapp.services', ['ionic'])

	.service('Booster', function($http, $cordovaDevice, $rootScope) {
	var $rs = $rootScope;
	//var baseUrl = "http://devb.theboosterapp.com/api/";
	var baseUrl = "http://mapapin.com/api/";
	//var baseUrl = "http://ws.theboosterapp.com/api/";

	this.register = function(fbId, firstName, lastName, email, dob) {
	
		var picUrl= "https://graph.facebook.com/" +  fbId + "/picture?type=square&height=200&width=200";

		return $http({
				method: 'POST',
				url: baseUrl + "User/Register",
				data: {
						FbUserId: fbId,
                        Email: email,
                        FirstName: firstName,
                        LastName: lastName,
                        PicUrl: picUrl
                    }
			});	
	}

	//use for e.g. FB Graph to get friends
	this.getUrl=function(theUrl)
	{
		return $http({
				method: 'GET',
				url: theUrl
			});		
	}

	this.updateDeviceId = function(userId, deviceId) {	    
		
		var qString="userId=" + userId + "&deviceId=" + deviceId + "&platform=" + $rs.getDevicePlatform(); //Note: for android "A", Ios "I";
		var theUrl = baseUrl + "User/UpdateDeviceId?" +  qString;
		if($rs.debug>0) console.log('updateDeviceId:theUrl='+theUrl);
		return $http({
				method: 'GET',
				url: theUrl
			});	
		
	}

	this.getProfileByFbId = function(fbId) {	
		if($rs.debug>0) console.log('services.getProfileByFbId:fbId='+fbId);    
		var qString="fbUserId=" + fbId ;
		return $http({
				method: 'GET',
				url: baseUrl + "User/GetProfileByFbUserId?" +  qString
			});	
		
	}
	
	this.friendJoined = function(myUserId, friends) {    		
		return $http({
				method: 'POST',
				url: baseUrl + "User/FriendJoined",
				data: {
						UserId: fromUserId,
						Friends: friends
                    }
		});	
	}

	this.getUpcomingBoost = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetUpcommingBoost?userId=" + userId
			});		
	}

	this.getRequestBoostCount = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetBoostRequestCount?userId=" + userId
			});		
	}

	this.getRequestBoost = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetBoostRequest?userId=" + userId
			});		
	}
	

	this.acceptBoostRequest = function(boostID) {		
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/AcceptBoost?boostId=" + boostID
			});		
	}	

	this.declineBoostRequest = function(boostId) {		
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/RejectBoost?boostId=" + boostId
			});		
	}



	this.getCompletedBoost = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetCompletedBoost?userId=" + userId
			});	
	}

	this.addBoost = function(fromUserId, toUserId, date, time, title, alert) {    		
		return $http({
				method: 'POST',
				url: baseUrl + "Boost/CreateBoostRequest",
				data: {
						UserId: fromUserId,
                        ToFbUserId: toUserId,
                        Title: title,
                        EventDate: date,
                        TzOffset: time,
                        Alert: alert
                    }
		});	
	}

	this.getBoostDetails = function(boostId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetBoostDetail?boostId=" + boostId
			});	
	}

	this.markNotfRead = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/MarkNotfRead?userId=" + userId
			});	
	}

	this.updateBoost = function(boostId, fromUserId, toUserId, dateTime, alert) {
		return $http({
				method: 'POST',
				url: baseUrl + "AddBoost",
				data: {
						boostId: boostId,
                        fromUserId: fromUserId,
                        toUserId: toUserId,
                        dateTime: dateTime,
                        alert: alert
                    }
			});	
	}

	this.addBoostReview = function(selfieId, comment, isLovedIt) {
		return $http({
				method: 'POST',
				url: baseUrl + "Boost/AddSelfieReview",
				data: {
                        BoostSelfieId: selfieId,
                        Comment: comment,
                        LovedIt: isLovedIt
                    }
			});	
	}

	this.sendSelfie = function(boostId, comment, imageData) {    		
		return $http({
				method: 'POST',
				url: baseUrl + "Boost/SendSelfie",
				data: {
						BoostId: boostId,
                        Comment: comment,
                        Image: imageData,
                    }
		});	
	}

	this.getSelfie = function(boostId) {
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetSelfieDetails?boostId=" + boostId
			});	
	}

	this.getNotification = function(userId) {	    
		return $http({
				method: 'GET',
				url: baseUrl + "Boost/GetNotficatios?userId=" + userId
			});	
	}

	this.sendPoke = function(boostId) {
		var theUrl = baseUrl + 'Boost/SendPoke?boostId='+boostId;
		//alert('sendPoke:theUrl=')+theUrl;
		return $http({
				method: 'GET',
				url: theUrl
			});	
	}

	this.getDefaultBoosters = function() {	    
		return $http({
				method: 'GET',
				url: baseUrl + "User/GetDefaultBooster"
			});	
	}


})

.service('PushNotifications', function(Utility, $ionicPlatform, $cordovaToast, $rootScope, $state) {

	var $rs = $rootScope;
	//alert('PushNotifications');

	var pushPlugin = null;

	/*
	this.deviceRegId = function() {
		$rootScope.getFDeviceId();
	}
	*/	

	this.initialize = function() {

		//alert('PushNotifications:initialize');

		//document.addEventListener('deviceready', function() {
		//$ionicPlatform.ready(function() {
			//if($rs.debug>0) console.log('PushNotifications:initialize:deviceready:device='+JSON.stringify(device));
			//$rs.setDeviceInfo(device);
			pushPlugin = window.plugins.pushNotification;
			///alert('pushPlugin='+pushPlugin);

			//TODO:!! FIX THIS
			//$rs.setDeviceInfo(JSON.stringify(ionic.Platform.device()));
			var dp = $rs.getDevicePlatform();
			//alert('initialize:$rs.dp='+dp);
			//alert('initialize:$rs.getDevicePlatform()='+$rs.getDevicePlatform());
			//TODO : solve undefined
			if ( dp == 'android' || dp == 'Android' || dp == "amazon-fireos" )
			{
				//alert('pushPlugin:not-ios:call:register:dp='+dp);
				pushPlugin.register(
					function(result) {          
						//alert('PushNotifications:initialize:2:result='+result);
						if($rs.debug>0) console.log('PushNotifications:initialize:1:result='+result);
					},
					function(result) {
						alert('PushNotifications:initialize:2:result='+result);
						if($rs.debug>0) console.log('PushNotifications:initialize:2:result='+JSON.stringify(result));
					},
					{
						"senderID":"220739435041",
						"ecb":"onNotificationGCM"
					});
				
			} else if ( dp == 'iOS' ) {
				//alert('pushPlugin:ios:call:register:dp='+dp);
				pushPlugin.register(
					function(result) {
						//  alert('pushPlugin.register:1:result='+JSON.stringify(result));
						if($rs.debug>0) console.log('pushPlugin.register:1:result='+JSON.stringify(result));
						$rootScope.setDeviceId(result)
					},
					function(result) {
						//alert('PushNotifications:initialize:2:result='+result);
						if($rs.debug>0) console.log('pushPlugin.register:2:result='+JSON.stringify(result));
					},
					{
						"badge":"true",
						"sound":"true",
						"alert":"true",
						"ecb":"onNotificationAPN"
					});
			}

		//});

		// notifications for Android
		window.onNotificationGCM = function(e) {
			//alert('onNotificationGCM:e='+JSON.stringify(e));
			window.boosterNotification = e;						
			switch( e.event )
		    {
		    case 'registered':
		        if ( e.regid.length > 0 )
		        {
		          $rootScope.setDeviceId(e.regid);
		        }
		    break;

		    case 'message':
		        // if this flag is set, this notification happened while we were in the foreground.
		        // you might want to play a sound to get the user's attention, throw up a dialog, etc.
		        if ( e.foreground )
		        {
		            // on Android soundname is outside the payload. 
		            // On Amazon FireOS all custom attributes are contained within payload
		            var soundfile = e.soundname || e.payload.sound;
		            // if the notification contains a soundname, play it.
		            //var my_media = new Media("/android_asset/www/"+ soundfile);
		            //my_media.play();
		        }
		        else
		        {  // otherwise we were launched because the user touched a notification in the notification tray.
		            if ( e.coldstart )
		            {
		                //
		            }
		            else
		            {
		                //
		            }
		        }

		        //navigator.notification.confirm("message", $rs.handleNotification, ["test title"], ["Ok", "Cancel"])
				
				$rs.sendToScreen(e.payload);

				/*
				var msg = e.payload.message.replace(/<b>/g, "")
				msg = msg.replace(/<\/b>/g, "");
				$cordovaToast.showShortCenter(msg).then(function(success) {
                        //$state.go('app.upcoming');
                        $rootScope.updateNotifications();
                      }, function (error) {
                        // error
                	}
                );
				*/

		       //if($rs.debug>0) console.log(e.payload.message); 
		       //Only works for GCM
		       // e.payload.msgcnt + '</li>');
		       //Only works on Amazon Fire OS
		       // e.payload.timeStamp
		    break;

		    case 'error':
		        //e.msg 
		    break;

		    default:
		        // Unknown
		    break;
		  }

		};

		// notifications for iOS
		window.onNotificationAPN = function(result) {
			//alert('onNotificationAPN:result='+JSON.stringify(result));
		    //if($rs.debug>0) console.log('onNotificationAPN:result='+JSON.stringify(result));

			/*
			if ( event.alert )
		    {
		        //navigator.notification.alert(event.alert);
		    }

		    if ( event.sound )
		    {
		        //var snd = new Media(event.sound);
		        //snd.play();
		    }

		    if ( event.badge )
		    {
		        //.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
		    }
		    */

		    $rs.sendToScreen(result);

		};

	};

	$rs.handleNotification=function(buttonIndex)
	{
		if($rs.debug>0) console.log('handleNotification:buttonIndex='+buttonIndex);

		if(buttonIndex == 2) // if CANCEL, return
		{
			return;
		}

		// else, goto screen
		if( $rs.varIsSet($rs.isAppStarted) )
		{
			if($rs.varIsSet($rs.pushState))
			{
				if($rs.varIsSet($rs.pushTarget))
				{
					$state.go($rs.pushState, {boostId: $rs.pushTarget});
				} else 
				{
					$state.go($rs.pushState);
				}
			}
		}
	}

	$rs.sendToScreen = function(result)
	{
		if($rs.debug>0) console.log('new sendToScreen:result='+JSON.stringify(result));
		var nType = result.nType;
		var targetId = result.targetId;
		var firstName = result.firstName;
		var boostName = result.boostName;
		if($rs.debug>0) console.log('sendToScreen:nType='+nType);
		if($rs.debug>0) console.log('sendToScreen:targetId='+targetId);
		if($rs.debug>0) console.log('sendToScreen:firstName='+firstName);
		if($rs.debug>0) console.log('sendToScreen:boostName='+boostName);

		var msg;
		if(nType == "AcceptBoost") {
			//r = window.confirm(''+firstName + ' accepted your request for '+boostName+'! Check it now?');
			msg = firstName + ' accepted your request for '+boostName+'! Check it now?';
			$rs.pushState = 'app.upcoming';
		} else if(nType == "Poke") {
			msg = firstName + ' Poked you to '+boostName+'! Check it now?';
			$rs.pushState = 'app.upcoming';
		} else if (nType == "RejectBoost") {
			msg = firstName + ' rejected your request for '+boostName+'! Check it now?';
		    $rs.pushState = 'app.upcoming';
		} else if (nType == "BoostRequest") {
			msg = firstName + ' requested a boost to '+boostName+'! Check it now?';
		    $rs.pushState = 'app.requests';
		} else if (nType == "Selfie") {
			msg = firstName + ' sent you a selfi for '+boostName+'! Check it now?';
		    $rs.pushState = 'app.completed';
		    //$rs.pushTarget= result.targetId;
		} else if (nType == "Review") {
			msg = firstName + ' sent you a review for '+boostName+'! Check it now?';
		    $rs.pushState = 'app.completedreview';
		    $rs.pushTarget= result.targetId;
		} else if (nType == "UpdateBoost") {
			msg = firstName + ' send you an update for '+boostName+'! Check it now?';
		    $rs.pushTarget= result.targetId;
		} else if (nType == "Time") {
			msg = 'It is time for '+firstName+' to '+boostName+'! Check it now?';
		}

		if($rs.debug>0) console.log("sendToScreen:$rs.pushState="+$rs.pushState);
		if($rs.debug>0) console.log("sendToScreen:$rs.pushTarget="+$rs.pushTarget);

		window.boosterNotification = result;

		navigator.notification.confirm(msg, $rs.handleNotification, nType, ["Ok", "Cancel"])

	}	


});

