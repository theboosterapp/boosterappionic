#bower install ngCordova

#cordova plugin add https://github.com/ccsoft/cordova-facebook
cordova plugins add org.apache.cordova.inappbrowser
cordova plugin add org.apache.cordova.console
cordova plugin add org.apache.cordova.camera
cordova plugin add https://github.com/VitaliiBlagodir/cordova-plugin-datepicker.git
cordova plugin add org.apache.cordova.device
cordova plugin add org.apache.cordova.dialogs
cordova plugin add org.apache.cordova.file
cordova plugin add https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin.git
cordova plugin add org.apache.cordova.statusbar
cordova plugin add https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin.git
cordova plugin add org.apache.cordova.network-information

#NEED FOR IOS 7 NO KB
cordova plugin add https://github.com/driftyco/ionic-plugins-keyboard.git
#NEED FOR POPUPS FOR EG PUSH HANDLING
cordova plugin add de.appplant.cordova.plugin.local-notification
#USE OUR PUSH PLUGIN
cordova plugin add https://github.com/adaptivedev/PushPlugin.git

cordova platform add ios@3.5.0 --usenpm
cordova platform add android@3.5.0 --usenpm

cordova -d plugin add ~/Documents/Dev/webapp/phonegap/Booster/phonegap-facebook-plugin/ --variable APP_ID="625654127541709" --variable APP_NAME="BoosterApp"


#cordova plugin add https://github.com/adaptivedev/cordova-facebook.git
#cordova plugin add https://github.com/christocracy/cordova-plugin-background-geolocation.git
#cordova plugin add org.apache.cordova.battery-status
#cordova plugin add org.apache.cordova.media-capture
#cordova plugin add org.apache.cordova.contacts
#cordova plugin add org.apache.cordova.geolocation
#cordova plugin add org.apache.cordova.globalization
#cordova plugin add https://github.com/SidneyS/cordova-plugin-nativeaudio.git
#cordova plugin add org.apache.cordova.media
#cordova plugin add https://github.com/Paldom/PinDialog.git
#cordova plugin add https://github.com/dferrell/plugins-application-preferences.git
#cordova plugin add org.pbernasconi.progressindicator
#cordova plugin add https://github.com/Paldom/SpinnerDialog.git
#cordova plugin add org.apache.cordova.vibration
#cordova plugin add https://github.com/MobileChromeApps/zip.git

##cordova plugin add https://github.com/ohh2ahh/AppAvailability.git
##cordova plugin add https://github.com/wildabeast/BarcodeScanner.git
##cordova plugin add https://github.com/VersoSolutions/CordovaClipboard
##cordova plugin add org.apache.cordova.device-motion
##cordova plugin add org.apache.cordova.device-orientation
##cordova plugin add https://github.com/danwilson/google-analytics-plugin.git
##cordova plugin add org.apache.cordova.splashscreen
##cordova plugin add https://github.com/brodysoft/Cordova-SQLitePlugin.git
